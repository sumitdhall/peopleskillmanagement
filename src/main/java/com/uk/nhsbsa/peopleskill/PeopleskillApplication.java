package com.uk.nhsbsa.peopleskill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.uk.nhsbsa.peopleskill.service.AddResourceAndSkillsImpl;

@SpringBootApplication
public class PeopleskillApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(PeopleskillApplication.class, args);
		System.out.println("SPring Boot Started");
		AddResourceAndSkillsImpl addResourcesAndSkills = context.getBean(AddResourceAndSkillsImpl.class);

	}
}
