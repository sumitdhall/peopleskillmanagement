package com.uk.nhsbsa.peopleskill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.nhsbsa.peopleskill.contract.ReadResourcesAndSkill;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;

public class ReadResourcesAndSkillImpl implements ReadResourcesAndSkill {

	final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

	@Autowired
	private PeopleRepository peopleRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	public List<PeopleDto> readAllResources() {
		List<PeopleDto> peopleDto = peopleRepository.read();
		if (!peopleDto.isEmpty()) {
			System.out.println("Below are the Mongo Db Results:: \n");
			peopleDto.forEach(System.out::println);
			return peopleDto;
		} else {
			return null;
		}
	}

}
