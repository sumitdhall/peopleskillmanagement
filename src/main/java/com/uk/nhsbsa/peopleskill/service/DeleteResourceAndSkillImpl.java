package com.uk.nhsbsa.peopleskill.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.uk.nhsbsa.peopleskill.contract.DeleteResourceAndSkill;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;

public class DeleteResourceAndSkillImpl implements DeleteResourceAndSkill {

	@Autowired
	private PeopleRepository peopleRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	public String deleteResource(String name) {
		List<PeopleDto> result = peopleRepository.delete(name);
		if (!result.isEmpty()) {
			return "Record deleted!!";
		}
		return "Record not found";
	}

}
