package com.uk.nhsbsa.peopleskill.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.mongodb.client.result.UpdateResult;
import com.uk.nhsbsa.peopleskill.contract.UpdateResourceSkill;
import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;

public class UpdateResourceSkillImpl implements UpdateResourceSkill {

	@Autowired
	private PeopleRepository peopleRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	public String updateResourceSkill(final String name, final List<Map<String, String>> skills) {
		UpdateResult status = peopleRepository.update(name, skills);
		if (status.getMatchedCount() > 0) {
			return "Updated Successfully!!";
		} else {
			return "Record not found";
		}
	}

}
