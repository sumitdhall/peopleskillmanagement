package com.uk.nhsbsa.peopleskill.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.uk.nhsbsa.peopleskill.contract.AddResourcesAndSkills;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;

public class AddResourceAndSkillsImpl implements AddResourcesAndSkills {

	@Autowired
	private PeopleRepository peopleRepository;

	public String addResourcesAndSkills(final PeopleDto peoplePayload) {
		PeopleDto creatResult = peopleRepository.create(peoplePayload);
		boolean status = creatResult.getResourceName().equals(peoplePayload.getResourceName());
		if (status) {
			return "Record added success!!";
		} else {
			return "Error Adding record";
		}
	}

}
