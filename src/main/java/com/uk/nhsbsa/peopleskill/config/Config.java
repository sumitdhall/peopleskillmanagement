package com.uk.nhsbsa.peopleskill.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;
import com.uk.nhsbsa.peopleskill.service.AddResourceAndSkillsImpl;
import com.uk.nhsbsa.peopleskill.service.DeleteResourceAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.ReadResourcesAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.UpdateResourceSkillImpl;

@Configuration
@ComponentScan("com.uk.nhsbsa.peopleskill")
public class Config {

	@Bean
	public AddResourceAndSkillsImpl addResourceAndSkillsImpl() {
		return new AddResourceAndSkillsImpl();
	}

	@Bean
	public ReadResourcesAndSkillImpl readResourcesImpl() {
		return new ReadResourcesAndSkillImpl();
	}

	@Bean
	public DeleteResourceAndSkillImpl deleteResourceAndSkillImpl() {
		return new DeleteResourceAndSkillImpl();
	}

	@Bean
	public UpdateResourceSkillImpl updateResourceSkillImpl() {
		return new UpdateResourceSkillImpl();
	}
	
}
