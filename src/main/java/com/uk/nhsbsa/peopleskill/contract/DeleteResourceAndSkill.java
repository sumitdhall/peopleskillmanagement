package com.uk.nhsbsa.peopleskill.contract;

public interface DeleteResourceAndSkill {
	public String deleteResource(String name);
}
