package com.uk.nhsbsa.peopleskill.contract;

import com.uk.nhsbsa.peopleskill.model.PeopleDto;

public interface AddResourcesAndSkills {
	String addResourcesAndSkills(PeopleDto peopleRequest);
}
