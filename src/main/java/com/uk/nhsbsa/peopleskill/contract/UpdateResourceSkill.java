package com.uk.nhsbsa.peopleskill.contract;

import java.util.List;
import java.util.Map;

public interface UpdateResourceSkill {
	public String updateResourceSkill(String name, List<Map<String, String>> skills);
}
