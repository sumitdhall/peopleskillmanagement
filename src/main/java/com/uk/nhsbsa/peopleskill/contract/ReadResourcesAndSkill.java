package com.uk.nhsbsa.peopleskill.contract;

import java.util.List;

import com.uk.nhsbsa.peopleskill.model.PeopleDto;

public interface ReadResourcesAndSkill {
	public List<PeopleDto> readAllResources();
}
