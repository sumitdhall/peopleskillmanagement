package com.uk.nhsbsa.peopleskill.model;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

//@Document Annotation to create collection in MongoDB with provided name
@Document("peopleskills")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PeopleDto {

	@Id
	private String id;
	
	@Field("people") //field name in mongo db
	@Indexed //indexed field in mongo db
	@JsonProperty("people") //json field name to receive in request payload
	private String resourceName;
	
	@Field("skill")
	@JsonProperty("skill")
	private List<Map<String, String>> skillSet;

	public PeopleDto(){
		
	}

	public PeopleDto(String resourceName, List<Map<String, String>> skillSet) {
		super();
		this.resourceName = resourceName;
		this.skillSet = skillSet;
	}


	public String getResourceName() {
		return resourceName;
	}


	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}


	public List<Map<String, String>> getSkillSet() {
		return skillSet;
	}


	public void setSkillSet(List<Map<String, String>> skillSet) {
		this.skillSet = skillSet;
	}


	//used ToStingBuilder for creating string
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

}
