package com.uk.nhsbsa.peopleskill.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.service.AddResourceAndSkillsImpl;
import com.uk.nhsbsa.peopleskill.service.DeleteResourceAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.ReadResourcesAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.UpdateResourceSkillImpl;

@RestController
public class ResourceController {

	@Autowired
	private AddResourceAndSkillsImpl addResourcesAndSkills;

	@Autowired
	private ReadResourcesAndSkillImpl readResourcesImpl;

	@Autowired
	private DeleteResourceAndSkillImpl deleteResourceAndSkillImpl;

	@Autowired
	private UpdateResourceSkillImpl updateResourceSkillImpl;

	@GetMapping
	@RequestMapping("/readAll")
	public List<PeopleDto> readAllResourcesAndSkills() {
		return readResourcesImpl.readAllResources();
	}

	@DeleteMapping
	@RequestMapping("/delete/{name}")
	public String deleteAllResourcesAndSkills(@PathVariable(value = "name") String name) {
		return deleteResourceAndSkillImpl.deleteResource(name);
	}

	@PostMapping
	@RequestMapping("/addResourceAndSkill")
	public String addResourceAndSkills(@RequestBody PeopleDto request) {
		return addResourcesAndSkills.addResourcesAndSkills(request);
	}

	@PutMapping
	@RequestMapping("/updateResourceSkill")
	public String updateAllResourceSkills(@RequestBody PeopleDto request) {
		return updateResourceSkillImpl.updateResourceSkill(request.getResourceName(), request.getSkillSet());
	}

}
