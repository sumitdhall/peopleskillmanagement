package com.uk.nhsbsa.peopleskill.repository;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.client.result.UpdateResult;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;

@Repository
public class PeopleRepository {

	@Autowired
	private MongoTemplate mongoTemplate;

	// below method uses mongo template class method insert to
	// save/create new record if it doesn't exists
	public PeopleDto create(final PeopleDto peopleRequest) {
		return mongoTemplate.insert(peopleRequest, "peopleskills");
	}

	// below method uses mongo template class method findAll to
	// search all the records in db
	public List<PeopleDto> read() {
		List<PeopleDto> peopleDto = mongoTemplate.findAll(PeopleDto.class);
		return peopleDto;
	}

	// below method uses mongo template class method updateMulti to
	// update all the record in db if it exists
	public UpdateResult update(final String name, final List<Map<String, String>> skills) {
		Query updateQuery = Query.query(Criteria.where("people").is(name));
		Update updateSkill = Update.update("skill", skills);
		return mongoTemplate.updateMulti(updateQuery, updateSkill, PeopleDto.class);
	}

	// below method uses mongo template class method findAllandRemove to
	// delete the record if it exists
	public List<PeopleDto> delete(String name) {
		Query findName = Query.query(Criteria.where("people").is(name));
		return mongoTemplate.findAllAndRemove(findName, PeopleDto.class);
	}

}
