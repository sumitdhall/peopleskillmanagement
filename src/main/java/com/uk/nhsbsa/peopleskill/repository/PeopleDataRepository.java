package com.uk.nhsbsa.peopleskill.repository;

//NOT USED IN THIS PROJECT

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

//Created PeopleDataRepository to store the People and Skills in temporary Java Hash Map Store.
//Not using this class anymore, using the MongoDB for CRUD operation. 
public class PeopleDataRepository {

	Map<String, List<Map<String, String>>> peopleStore = new HashMap<>();

	public String addToPeopleStore(final String people, final List<Map<String, String>> peopleSkills) {

		if (null != people && null != peopleSkills) {
			peopleStore.put(people, peopleSkills);
			System.out.println("Added Successfully to Store!!!");
			return "Success";
		}
		return "Failure";
	}

	public Map<String, List<Map<String, String>>> readAllPeople() {
		System.out.println("Inside the readAllPeople method");

		Set<String> keySet = peopleStore.keySet();
		ArrayList<String> listOfKeys = new ArrayList<String>(keySet);

		Collection<List<Map<String, String>>> values = peopleStore.values();

		return peopleStore;
	}

	public String deletePeople(final String name) {
		System.out.println("Inside the deletePeople method");
		boolean deleted = peopleStore.entrySet().removeIf(entry -> entry.getKey().equals(name));
		if (deleted) {
			return "Deleted people";
		} else {
			return "Record Not Found";
		}
	}

	public String updatePeopleStore(String peopleName, List<Map<String, String>> skills) {
		boolean deleted = peopleStore.entrySet().removeIf(entry -> entry.getKey().equals(peopleName));
		if (deleted) {
			peopleStore.put(peopleName, skills);
			return "Skills updated";
		}
		return "Not Found";

	}

	public void printPeopleStore() {
		if (!peopleStore.isEmpty()) {
			System.out.println("Inside the People Store Print method");
			System.out.println("Person Name  and Skill Set: ");
			peopleStore.forEach((k, v) -> {
				System.out.println(k);
				v.forEach(System.out::println);
			});

		} else {
			System.out.print("Store is empty");
		}
	}
}
