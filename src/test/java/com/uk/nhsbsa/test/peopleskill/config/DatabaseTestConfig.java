package com.uk.nhsbsa.test.peopleskill.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.uk.nhsbsa.peopleskill.repository.PeopleRepository;
import com.uk.nhsbsa.peopleskill.service.AddResourceAndSkillsImpl;
import com.uk.nhsbsa.peopleskill.service.DeleteResourceAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.ReadResourcesAndSkillImpl;
import com.uk.nhsbsa.peopleskill.service.UpdateResourceSkillImpl;

@org.springframework.boot.test.context.TestConfiguration
public class DatabaseTestConfig {
	
	@Bean
	public DeleteResourceAndSkillImpl deleteResourceAndSkillImpl() {
		return new DeleteResourceAndSkillImpl();
	}
	
	@Bean
	public PeopleRepository peopleRepository() {
		return new PeopleRepository();
	}
	
	@Bean
	public AddResourceAndSkillsImpl addResourceAndSkillsImpl() {
		return new AddResourceAndSkillsImpl();
	}
	
	@Bean
	public ReadResourcesAndSkillImpl readResourcesAndSkillImpl() {
		return new ReadResourcesAndSkillImpl();
	}
	@Bean
	public UpdateResourceSkillImpl updateResourceSkillImpl() {
		return new UpdateResourceSkillImpl();
	}
	
}
