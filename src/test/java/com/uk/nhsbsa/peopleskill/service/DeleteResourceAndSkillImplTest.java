package com.uk.nhsbsa.peopleskill.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.service.DeleteResourceAndSkillImpl;
import com.uk.nhsbsa.test.peopleskill.config.DatabaseTestConfig;

import jdk.jfr.Category;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@Category("integration")
@Import(DatabaseTestConfig.class)
public class DeleteResourceAndSkillImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String PEOPLE_SKILL_JSON = "{\r\n" + "	\"people\": \"Sumit\",\r\n" + "	\"skill\": [{\r\n"
			+ "		\"Skill1\": \"level2\"\r\n" + "	}, {\r\n" + "		\"Skill2\": \"level3\"\r\n" + "	}]\r\n" + "}";
	private static final String EXPECTED_RESULT = "Record deleted!!";
	private static String result;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	DeleteResourceAndSkillImpl deleteResourceAndSkillImpl;

	PeopleDto peopleJson;

	@Test
	public void givenPeopleAndSkillArePresent_whenThePeopleWithNameIsDeleted_thenVerifyPeopleSkillAndResourcesAreDeleted() {
		givenPeopleAndSkillArePresent();

		whenThePeopleWithNameIsDeleted();

		thenVerifyPeopleSkillAndResourcesAreDeleted();
	}

	private void givenPeopleAndSkillArePresent() {
		try {
			// creating the people object to add into mongo collection
			peopleJson = OBJECT_MAPPER.readValue(PEOPLE_SKILL_JSON, PeopleDto.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		this.mongoTemplate.insert(peopleJson);
	}

	private void whenThePeopleWithNameIsDeleted() {
		// calling the delete service to delete the people and skills using the name
		result = this.deleteResourceAndSkillImpl.deleteResource("Sumit");
		System.out.println("Result of Deletion ::" + result);
	}

	private void thenVerifyPeopleSkillAndResourcesAreDeleted() {
		// then verifying
		assertThat(result).isNotBlank().isEqualToIgnoringCase(EXPECTED_RESULT);
	}

	@AfterEach
	public void afterEach() {
		// tearing down the DB after each test run to maintain the consistency for other
		// test cases
		this.mongoTemplate.dropCollection(PeopleDto.class);
	}

}
