package com.uk.nhsbsa.peopleskill.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.service.AddResourceAndSkillsImpl;
import com.uk.nhsbsa.test.peopleskill.config.DatabaseTestConfig;

import jdk.jfr.Category;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@Category("integration")
@Import(DatabaseTestConfig.class)
public class AddResourceAndSkillImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String PEOPLE_SKILL_JSON = "{\r\n" + "	\"people\": \"SumitDhall\",\r\n"
			+ "	\"skill\": [{\r\n" + "		\"Java\": \"Expert\"\r\n" + "	}, {\r\n"
			+ "		\"Docker\": \"Practitioner\"\r\n" + "	},{\r\n" + "        \"mongoDB\":\"Working\"\r\n"
			+ "    },{\r\n" + "        \"Thymeleaf\":\"Awareness\"\r\n" + "    },{\r\n"
			+ "        \"Management\":\"Working\"\r\n" + "    },{\r\n" + "        \"TeamPlayer\":\"Expert\"\r\n"
			+ "    },{\r\n" + "        \"OracleSQL\":\"Working\"\r\n" + "    },{\r\n"
			+ "        \"AWS\":\"Practitioner\"\r\n" + "    }]\r\n" + "}";
	private static final String EXPECTED_RESULT = "Record added success!!";
	private static String result;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	AddResourceAndSkillsImpl addResourceAndSkillsImpl;

	PeopleDto peopleJson;

	@Test
	public void givenPeopleAndSkills_whenThePeopleWithSkillsAreAdded_thenVerifyPeopleSkillAndResourcesArePresent() {
		givenPeopleAndSkills();

		whenThePeopleWithSkillsAreAdded();

		thenVerifyPeopleSkillAndResourcesArePresent();
	}

	private void givenPeopleAndSkills() {
		try {
			// creating the people object to add into mongo collection
			peopleJson = OBJECT_MAPPER.readValue(PEOPLE_SKILL_JSON, PeopleDto.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	private void whenThePeopleWithSkillsAreAdded() {
		// calling the addition service to add the people and skills
		result = this.addResourceAndSkillsImpl.addResourcesAndSkills(peopleJson);
		System.out.println("Result of addition ::" + result);
	}

	private void thenVerifyPeopleSkillAndResourcesArePresent() {
		// then verifying
		assertThat(result).isNotBlank().isEqualToIgnoringCase(EXPECTED_RESULT);
	}

	@AfterEach
	public void afterEach() {
		// tearing down the DB after each test run to maintain the consistency for other
		// test cases
		this.mongoTemplate.dropCollection(PeopleDto.class);
	}

}
