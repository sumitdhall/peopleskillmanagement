package com.uk.nhsbsa.peopleskill.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.service.ReadResourcesAndSkillImpl;
import com.uk.nhsbsa.test.peopleskill.config.DatabaseTestConfig;

import jdk.jfr.Category;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@Category("integration")
@Import(DatabaseTestConfig.class)
public class ReadResourcesAndSkillImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String PEOPLE_SKILL_JSON = "{\r\n" + "	\"people\": \"SumitDhall\",\r\n"
			+ "	\"skill\": [{\r\n" + "		\"Java\": \"Expert\"\r\n" + "	}, {\r\n"
			+ "		\"Docker\": \"Practitioner\"\r\n" + "	},{\r\n" + "        \"mongoDB\":\"Working\"\r\n"
			+ "    },{\r\n" + "        \"Thymeleaf\":\"Awareness\"\r\n" + "    },{\r\n"
			+ "        \"Management\":\"Working\"\r\n" + "    },{\r\n" + "        \"TeamPlayer\":\"Expert\"\r\n"
			+ "    },{\r\n" + "        \"OracleSQL\":\"Working\"\r\n" + "    },{\r\n"
			+ "        \"AWS\":\"Practitioner\"\r\n" + "    }]\r\n" + "}";
	Map<String, String>map = new HashMap();
	private static List<PeopleDto> result;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	ReadResourcesAndSkillImpl readResourcesAndSkillImpl;

	PeopleDto people;
	
	List<PeopleDto> peopleList;

	@Test
	public void givenPeopleAndSkillsArePresent_whenReadingAllThePeopleWithSkills_thenVerifyPeopleSkillAndResourcesAreReturned() {
		givenPeopleAndSkillsArePresent();

		whenReadingAllThePeopleWithSkills();

		thenVerifyPeopleSkillAndResourcesAreReturned();
	}

	@SuppressWarnings("rawtypes")
	private void givenPeopleAndSkillsArePresent() {
		try {
			// creating the people1, people2, people3 object to add into mongo collection
			people = OBJECT_MAPPER.readValue(PEOPLE_SKILL_JSON, PeopleDto.class);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		peopleList = new ArrayList();
		peopleList.add(people);
		this.mongoTemplate.insertAll(peopleList);
	}

	private void whenReadingAllThePeopleWithSkills() {
		// calling the read service to read all the people and skills
		result = this.readResourcesAndSkillImpl.readAllResources();
	}

	private void thenVerifyPeopleSkillAndResourcesAreReturned() {
		// then verifying
		assertThat(result).size().isEqualTo(1);
		assertThat(result).extracting("class").contains(PeopleDto.class);
		assertThat(result).extracting((record) -> record.getResourceName()).contains("SumitDhall");
		
		map.put("Java", "Expert");
		assertThat(result).extracting((record) -> record.getSkillSet().get(0)).contains(map);		
	}

	@AfterEach
	public void afterEach() {
		// tearing down the DB after each test run to maintain the consistency for other
		// test cases
		this.mongoTemplate.dropCollection(PeopleDto.class);
	}

}
