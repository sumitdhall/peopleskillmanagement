package com.uk.nhsbsa.peopleskill.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uk.nhsbsa.peopleskill.model.PeopleDto;
import com.uk.nhsbsa.peopleskill.service.UpdateResourceSkillImpl;
import com.uk.nhsbsa.test.peopleskill.config.DatabaseTestConfig;

import jdk.jfr.Category;

@DataMongoTest
@ExtendWith(SpringExtension.class)
@Category("integration")
@Import(DatabaseTestConfig.class)
public class UpdateResourceSkillImplTest {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static final String PEOPLE_SKILL_JSON = "{\r\n" + "	\"people\": \"SumitDhall\",\r\n"
			+ "	\"skill\": [{\r\n" + "		\"Java\": \"Expert\"\r\n" + "	}, {\r\n"
			+ "		\"Docker\": \"Practitioner\"\r\n" + "	},{\r\n" + "        \"mongoDB\":\"Working\"\r\n"
			+ "    },{\r\n" + "        \"Thymeleaf\":\"Awareness\"\r\n" + "    },{\r\n"
			+ "        \"Management\":\"Working\"\r\n" + "    },{\r\n" + "        \"TeamPlayer\":\"Expert\"\r\n"
			+ "    },{\r\n" + "        \"OracleSQL\":\"Working\"\r\n" + "    },{\r\n"
			+ "        \"AWS\":\"Practitioner\"\r\n" + "    }]\r\n" + "}";
	private static final String EXPECTED_RESULT = "Updated Successfully!!";
	private static String result;

	@Autowired
	MongoTemplate mongoTemplate;

	@Autowired
	UpdateResourceSkillImpl updateResourceSkillImpl;

	PeopleDto peopleJson;
	List<Map<String, String>> skillList = new ArrayList();
	Map<String, String> map = new HashMap();

	@Test
	public void givenPeopleAndSkillsToBeUpdated_whenThePeopleWithSkillsAreUpdated_thenVerifyPeopleSkillAreUpdated() {
		givenPeopleAndSkillsToBeUpdated();

		whenThePeopleWithSkillsAreUpdated();

		thenVerifyPeopleSkillAreUpdated();
	}

	private void givenPeopleAndSkillsToBeUpdated() {
		try {
			// creating the people object to add sample mongo template in collection
			peopleJson = OBJECT_MAPPER.readValue(PEOPLE_SKILL_JSON, PeopleDto.class);
			this.mongoTemplate.insert(peopleJson);
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		map.put("OracleSQL", "Working");
		map.put("AWS", "Practitioner");
		skillList.add(map);

	}

	private void whenThePeopleWithSkillsAreUpdated() {
		// calling the update service to update the people and skills
		result = this.updateResourceSkillImpl.updateResourceSkill("SumitDhall", skillList);
	}

	private void thenVerifyPeopleSkillAreUpdated() {
		// then verifying
		assertThat(result).isNotBlank().isEqualToIgnoringCase(EXPECTED_RESULT);
	}

	@AfterEach
	public void afterEach() {
		// tearing down the DB after each test run to maintain the consistency for other
		// test cases
		this.mongoTemplate.dropCollection(PeopleDto.class);
	}

}