# peopleskillmanagement

This app manages people and their skill.

People can add as many skills as they want, can update the skills, can delete the skills, can read all the skills.

This app exposes 4 Rest end points:
1) Create: http://localhost:8080/addResourceAndSkill
2) Update: http://localhost:8080/updateResourceSkill
3) Read: http://localhost:8080/readAll
4) Delete: http://localhost:8080/delete/{name}

This app accepts PeopleDto Json payload containing, person name and array of skills along with the levels.
Below is sample Json object for input:
{
	"people": "SumitDhall",
	"skill": [{
		"Java": "Expert"
	}, {
		"Docker": "Practitioner"
	},{
        "mongoDB":"Working"
    },{
        "Thymeleaf":"Awareness"
    },{
        "Management":"Working"
    },{
        "TeamPlayer":"Expert"
    },{
        "OracleSQL":"Working"
    },{
        "AWS":"Practitioner"
    }]
}

Main class for the application is "PeopleskillApplication".

Feel free to explore the code.